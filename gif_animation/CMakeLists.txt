cmake_minimum_required(VERSION 3.5)

# Compilation flags
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall --pedantic -std=c99")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall --pedantic -std=c++11")

if(CONFIG_LIBMANGO_GN)
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -DGNEMU")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DGNEMU")
endif(CONFIG_LIBMANGO_GN)

# Set DEBUG compilation
set(CMAKE_BUILD_TYPE Debug)

set(MANGO_ROOT "$ENV{MANGO_ROOT}" CACHE STRING "MANGO installation root directory")

set(gif_animation_SOURCES main.cpp AnimatedGifSaver.cpp kernel_runner.cpp)

add_executable (gif_animation ${gif_animation_SOURCES})


target_include_directories(gif_animation PUBLIC ${MANGO_ROOT}/include/libmango/)
target_include_directories(gif_animation PUBLIC ${MANGO_ROOT}/include/)

if (NOT BOSP_PATH) 
	message(FATAL_ERROR "You have to specify -DBOSP_PATH=\"/path/to/bbque\"!")
endif (NOT BOSP_PATH)

target_include_directories(gif_animation PUBLIC ${BOSP_PATH}/include/bbque/)
target_include_directories(gif_animation PUBLIC ${BOSP_PATH}/include/)
target_link_libraries(gif_animation PUBLIC ${BOSP_PATH}/lib/bbque/libbbque_pms.so)
target_link_libraries(gif_animation PUBLIC ${BOSP_PATH}/lib/bbque/libbbque_tg.so)


find_library(MANGO_LIBRARY
	NAMES mango
	HINTS "${MANGO_ROOT}/lib")

find_library(GIF_LIB
	NAMES gif)

target_link_libraries(gif_animation PUBLIC ${MANGO_LIBRARY})
target_link_libraries(gif_animation PUBLIC ${GIF_LIB})

install(TARGETS gif_animation DESTINATION ${MANGO_ROOT}/usr/bin)
install(FILES gif_animation.recipe DESTINATION ${BOSP_PATH}/etc/bbque/recipes)

add_subdirectory(copy_kernel)
add_subdirectory(scale_kernel)
add_subdirectory(smooth_kernel)


