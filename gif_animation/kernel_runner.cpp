#include "kernel_runner.h"
#include "host/logger.h"

KernelRunner::KernelRunner(int SX, int SY)
{
    mango::mango_init_logger();
    // Initialization
    mango_rt = new mango::BBQContext("gif_animation", "gif_animation");

    const char *mango_root = getenv("MANGO_ROOT");
    std::string kf_path;

    if (mango_root == NULL) {
        kf_path = "/opt/mango";
    } else {
        kf_path = mango_root;
    }


#ifdef GNEMU
  char scale_kernel_file[] = "/usr/local/share/gif_animation/scale/scale_kernel";
  char smooth_kernel_file[] = "/usr/local/share/gif_animation/smooth/smooth_kernel";
  char copy_kernel_file[] = "/usr/local/share/gif_animation/copy/copy_kernel";
#else
  char scale_kernel_file[] = "/usr/local/share/gif_animation/scale/memory.data.fpga.datafile";
  char smooth_kernel_file[] ="/usr/local/share/gif_animation/copy/memory.data.fpga.datafile";
  char copy_kernel_file[] =  "/usr/local/share/gif_animation/smooth/memory.data.fpga.datafile";
#endif

#ifdef GNEMU
    auto kf_scale = new mango::KernelFunction();
    kf_scale->load(kf_path + scale_kernel_file,
                   mango::UnitType::GN, mango::FileType::BINARY);
    auto kf_copy = new mango::KernelFunction();
    kf_copy->load(kf_path + copy_kernel_file,
                  mango::UnitType::GN, mango::FileType::BINARY);
    auto kf_smooth = new mango::KernelFunction();
    kf_smooth->load(kf_path + smooth_kernel_file,
                    mango::UnitType::GN, mango::FileType::BINARY);
#else
    auto kf_scale = new mango::KernelFunction();
    kf_scale->load(kf_path + scale_kernel_file,
                   mango::UnitType::PEAK, mango::FileType::BINARY);
    auto kf_copy = new mango::KernelFunction();
    kf_copy->load(kf_path + copy_kernel_file,
                  mango::UnitType::PEAK, mango::FileType::BINARY);
    auto kf_smooth = new mango::KernelFunction();
    kf_smooth->load(kf_path + smooth_kernel_file,
                    mango::UnitType::PEAK, mango::FileType::BINARY);

#endif

    // Registration of task graph
    auto kscale  = mango_rt->register_kernel(KSCALE, kf_scale, {B1}, {B2});
    auto kcopy   = mango_rt->register_kernel(KCOPY, kf_copy, {B2}, {B3});
    auto ksmooth = mango_rt->register_kernel(KSMOOTH, kf_smooth, {B2}, {B3});

    auto b1 = mango_rt->register_buffer(B1,
                                        SX*SY*3*sizeof(Byte), {HOST}, {KSCALE});
    auto b2 = mango_rt->register_buffer(B2,
                                        SX*2*SY*2*3*sizeof(Byte), {KSCALE}, {KCOPY, KSMOOTH});
    auto b3 = mango_rt->register_buffer(B3,
                                        SX*2*SY*2*3*sizeof(Byte), {KCOPY, KSMOOTH}, {HOST});

    tg = new mango::TaskGraph({ kscale, kcopy, ksmooth }, { b1, b2, b3 });

    // Resource Allocation
    mango_rt->resource_allocation(*tg);

    // Execution setup
    auto argB1 = new mango::BufferArg( b1 );
    auto argB2 = new mango::BufferArg( b2 );
    auto argB3 = new mango::BufferArg( b3 );
    auto argSX = new mango::ScalarArg<int>( SX );
    auto argSY = new mango::ScalarArg<int>( SY );
    auto argSX2 = new mango::ScalarArg<int>( SX*2 );
    auto argSY2 = new mango::ScalarArg<int>( SY*2 );

    argsKSCALE = new mango::KernelArguments(
    { argB2, argB1, argSX, argSY }, kscale);
    argsKCOPY = new mango::KernelArguments(
    { argB3, argB2, argSX2, argSY2 }, kcopy);
    argsKSMOOTH = new mango::KernelArguments(
    { argB3, argB2, argSX2, argSY2 }, ksmooth);
}

KernelRunner::~KernelRunner()
{
    // Deallocation and teardown
    mango_rt->resource_deallocation(*tg);
}

void KernelRunner::run_kernel(Byte *out, Byte *in)
{
    auto b1 = mango_rt->get_buffer(B1);
    auto b3 = mango_rt->get_buffer(B3);
    auto kscale  = mango_rt->get_kernel(KSCALE);
    auto kcopy   = mango_rt->get_kernel(KCOPY);
    auto ksmooth = mango_rt->get_kernel(KSMOOTH);

    // Data transfer and kernel execution
    b1->write(in);

    auto e1=mango_rt->start_kernel(kscale, *argsKSCALE);
    e1->wait();

    auto e2=mango_rt->start_kernel(kcopy, *argsKCOPY);
    e2->wait();

    auto e3=mango_rt->start_kernel(ksmooth, *argsKSMOOTH);
    e3->wait();

	fprintf(stderr, "After smooth kernel\n");
	fflush(stderr);


    b3->read(out);
}
