cmake_minimum_required(VERSION 3.5)

# Compilation flags
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall --pedantic -std=c99")

if(CONFIG_LIBMANGO_GN)
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -DGNEMU")
endif(CONFIG_LIBMANGO_GN)

# Set DEBUG compilation
set(CMAKE_BUILD_TYPE Debug)

set(MANGO_ROOT "$ENV{MANGO_ROOT}" CACHE STRING "MANGO installation root directory")

set(SYNC_SOURCES sync.c)


add_executable (sync ${SYNC_SOURCES})


target_include_directories(sync PUBLIC ${MANGO_ROOT}/include/libmango/)

if (NOT BOSP_PATH) 
	message(FATAL_ERROR "You have to specify -DBOSP_PATH=\"/path/to/bbque\"!")
endif (NOT BOSP_PATH)

target_link_libraries(sync PUBLIC ${BOSP_PATH}/lib/bbque/libbbque_pms.so)
target_link_libraries(sync PUBLIC ${BOSP_PATH}/lib/bbque/libbbque_tg.so)


find_library(MANGO_LIBRARY
	NAMES mango
	HINTS "${MANGO_ROOT}/lib")

#target_link_libraries(sync PUBLIC ${HN_LIBRARY})
target_link_libraries(sync PUBLIC ${MANGO_LIBRARY})

install(TARGETS sync DESTINATION ${MANGO_ROOT}/usr/bin)

add_subdirectory(kernel)

