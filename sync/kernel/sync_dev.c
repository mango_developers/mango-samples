/* 
 *
 * Test application. This application
 * will launch a kernel in PEAK. The kernel will just
 * compute a matrix multiplication. The size of the matrix 
 * and the matrices pointers will be passed as parameters
 *
 * After executing the kernel, the application shows
 * the resulting output matrix
 *
*/
#include "dev/mango_hn.h"
#include "dev/debug.h"
#include <stdlib.h>


#pragma mango_kernel
void kernel_function(int n, mango_event_t e, int* B) {

	for (int i=0; i < n; i++) {
		printf("DEV: Signaling %d (%d/%d)...\n", 1, i, n);
		mango_write_synchronization(&e, 1);
		printf("DEV: Waiting %d...\n", 2);
		mango_wait(&e, 2);
	}

	*B = n;

	return;
}

