
/*
Example of usage of AnimatedGifSaver class.
Saves a small looped 0-1-2-3 animation in file "0123.gif".
From https://sourceforge.net/p/giflib/patches/6/
*/

#include "AnimatedGifSaver.h"
#include <stdio.h>
#include "kernel_runner.h"


// red and white RGB pixels
#define R 255,0,0
#define W 255,255,255

// Lets define a few frames for this little demo...

// ...red and white RGB pixels
#define R 255,0,0
#define W 255,255,255

// ...frames sizes
const int  SX=5;
const int  SY=7;

// ...and, the frames themselves
// (note: they are defined bottom-to-top (a-la OpenGL) so they appear upside-down).

Byte frames[4][SX*SY*3] = { {
        W,W,W,W,W,
        W,W,R,W,W,
        W,R,W,R,W,
        W,R,W,R,W,
        W,R,W,R,W,
        W,W,R,W,W,
        W,W,W,W,W,
    },
    {
        W,W,W,W,W,
        W,W,R,W,W,
        W,W,R,W,W,
        W,W,R,W,W,
        W,R,R,W,W,
        W,W,R,W,W,
        W,W,W,W,W,
    },
    {
        W,W,W,W,W,
        W,R,R,R,W,
        W,R,W,W,W,
        W,W,R,W,W,
        W,W,W,R,W,
        W,R,R,W,W,
        W,W,W,W,W,
    },
    {
        W,W,W,W,W,
        W,R,R,W,W,
        W,W,W,R,W,
        W,W,R,W,W,
        W,W,W,R,W,
        W,R,R,W,W,
        W,W,W,W,W,
    }
};

/* These variables and functions are added to support the scale functionality.
 */
Byte sframes[4][SX*2*SY*2*3];

void double_frame(Byte *out, Byte *in, int X, int Y)
{
    int X2=X*2;
    int Y2=Y*2;
    for(int x=0; x<X2; x++)
        for(int y=0; y<Y2; y++)
            for(int c=0; c<3; c++) {
                out[y*X2*3+x*3+c]=in[y/2*X*3+x/2*3+c];
            }
}

int main()
{
    AnimatedGifSaver saver(SX*2,SY*2);

    printf("Starting setup\n");
    auto kr=KernelRunner(SX,SY);
    printf("Running frames\n");
    kr.run_kernel((Byte *)sframes, (Byte *)frames);
    printf("All frames processed\n");
    for(int i=0; i<4; i++)
        saver.AddFrame(sframes[i],i+1);
    saver.Save("0123_kernel.gif");
}
