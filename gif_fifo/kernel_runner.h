#ifndef KERNEL_RUNNER_H
#define KERNEL_RUNNER_H
#include "host/mango.h"

typedef unsigned char Byte;

class KernelRunner
{
private:
    mango::BBQContext *mango_rt;
    mango::KernelArguments *argsKSCALE;
    mango::TaskGraph *tg;
    enum { HOST=0, KSCALE };
    enum { B1=1, B2 };
    int  SX;
    int  SY;

public:
    KernelRunner(int SX, int SY);

    ~KernelRunner();

    void run_kernel(Byte *out, Byte *in);
};

#endif
